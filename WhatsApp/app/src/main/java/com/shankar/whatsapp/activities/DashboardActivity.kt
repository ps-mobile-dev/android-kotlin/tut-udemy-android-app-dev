package com.shankar.whatsapp.activities

import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.shankar.whatsapp.R
import com.shankar.whatsapp.adapters.SectionPagerAdapter
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    var sectionPagerAdapter: SectionPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        supportActionBar!!.title = "Dashboard"

        sectionPagerAdapter = SectionPagerAdapter(supportFragmentManager)
        dashboardViewPagerID.adapter = sectionPagerAdapter
        mainTabs.setupWithViewPager(dashboardViewPagerID)
        mainTabs.setTabTextColors(Color.WHITE, Color.BLACK)

//        if (intent.extras != null){
//            var username = intent.extras.get("display_name")
//            Toast.makeText(this, "Signed in as : $username", Toast.LENGTH_SHORT).show()
//        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        super.onOptionsItemSelected(item)
        if(item != null){
            when(item.itemId){
                R.id.settingsID -> {
                    //Show settings
                    startActivity(Intent(this, SettingsActivity::class.java))
                }
                R.id.logoutID -> {
                    //Log user out
                    FirebaseAuth.getInstance().signOut()
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()
                }
            }
        }
        return true
    }
}
