package com.shankar.whatsapp.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.shankar.whatsapp.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    var mAuth: FirebaseAuth? = null
    var mDatabase: DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mAuth = FirebaseAuth.getInstance()

        loginButton.setOnClickListener {
            var email = loginEmailET.text.toString().trim()
            var password = loginPasswordET.text.toString().trim()
            if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(email)) {
                loginUser(email, password)
            } else {
                Toast.makeText(this, "None of the fields can be empty", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun loginUser(email: String, password: String) {
        mAuth!!.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener { task: Task<AuthResult> ->
                    if (task.isSuccessful) {
                        var displayName = email.split("@").get(0)
                        var dashboardIntent = Intent(this, DashboardActivity::class.java)
                        dashboardIntent.putExtra("display_name", displayName)
                        startActivity(dashboardIntent)
                        finish()
                    } else {
                        Toast.makeText(this, "Login Failed! Try Again.", Toast.LENGTH_SHORT).show()
                    }
                }
    }
}
