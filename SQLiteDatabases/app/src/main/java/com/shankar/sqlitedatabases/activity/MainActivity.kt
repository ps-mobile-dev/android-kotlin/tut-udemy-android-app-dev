package com.shankar.sqlitedatabases.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.widget.Toast
import com.shankar.sqlitedatabases.R
import com.shankar.sqlitedatabases.data.ChoresDatabaseHandler
import com.shankar.sqlitedatabases.model.Chore
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    var dbHandler: ChoresDatabaseHandler? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        dbHandler = ChoresDatabaseHandler(this)
        checkDB()

        saveChore.setOnClickListener {
            if (!TextUtils.isEmpty(enterChoreId.text.trim())
                    && !TextUtils.isEmpty(assignToId.text.trim())
                    && !TextUtils.isEmpty(assignedById.text.trim())) {
                var chore = Chore()
                chore.choreName = enterChoreId.text.toString().trim()
                chore.assignedTo = assignToId.text.toString().trim()
                chore.assignedBy = assignedById.text.toString().trim()
                saveToDB(chore)
            } else {
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        checkDB()
    }

    //If DB is not empty then you are redirected to ChoreListActivity
    fun checkDB(){
        if (dbHandler!!.getChoresCount() > 0){
            startActivity(Intent(this, ChoreListActivity::class.java))
            finish()
        }
    }

    fun saveToDB(chore: Chore) {
        dbHandler!!.createChore(chore)
        checkDB()
    }
}
