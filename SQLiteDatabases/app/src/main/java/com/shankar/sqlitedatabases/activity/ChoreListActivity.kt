package com.shankar.sqlitedatabases.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.shankar.sqlitedatabases.R
import com.shankar.sqlitedatabases.data.ChoreListAdapter
import com.shankar.sqlitedatabases.data.ChoresDatabaseHandler
import com.shankar.sqlitedatabases.model.Chore
import kotlinx.android.synthetic.main.activity_chore_list.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.popup.view.*

class ChoreListActivity : AppCompatActivity() {

    private var adapter: ChoreListAdapter? = null
    private var choreList: ArrayList<Chore>? = null
    private var choreListItems: ArrayList<Chore>? = null
    private var layoutManager: RecyclerView.LayoutManager? = null
    private var dialogBuilder: AlertDialog.Builder? = null
    private var dialog: AlertDialog? = null
    var dbHandler: ChoresDatabaseHandler? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chore_list)

        dbHandler = ChoresDatabaseHandler(this)

        choreList = ArrayList<Chore>()
        choreListItems = ArrayList()
        layoutManager = LinearLayoutManager(this)
        adapter = ChoreListAdapter(choreListItems!!,this)

        //Setup recycler view
        recyclerViewChores.layoutManager = layoutManager
        recyclerViewChores.adapter = adapter

        //Loading Chores
        choreList = dbHandler!!.readChores()
        choreList!!.reverse()

        for(c in choreList!!.iterator()){
            val chore = Chore()
            chore.choreId = c.choreId
            chore.choreName = c.choreName
            chore.assignedTo = c.assignedTo
            chore.assignedBy = c.assignedBy
            chore.timeAssigned = c.timeAssigned
            choreListItems!!.add(chore)
        }
        adapter!!.notifyDataSetChanged()

    }

    //To show menu on action bar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.top_menu,menu)
        return true
    }

    //To implement click listner on menu items in actionbar
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
         if(item!!.itemId == R.id.add_menu_button){
            createDialog()
         }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    fun createDialog(){
        var view = layoutInflater.inflate(R.layout.popup, null)
        var choreName = view.popEnterChore
        var assignedBy = view.popAssignedBy
        var assignedTo = view.popAssignTo
        var saveButton = view.popSaveChore
        dialogBuilder = AlertDialog.Builder(this).setView(view)
        dialog = dialogBuilder!!.create()
        dialog!!.show()
        saveButton.setOnClickListener {
            if (!TextUtils.isEmpty(choreName.text.trim())
                    && !TextUtils.isEmpty(assignedBy.text.trim())
                    && !TextUtils.isEmpty(assignedTo.text.trim())) {
                var chore = Chore()
                chore.choreName = choreName.text.toString().trim()
                chore.assignedTo = assignedTo.text.toString().trim()
                chore.assignedBy = assignedBy.text.toString().trim()
                saveToDB(chore)
                dialog!!.dismiss()
                //reloading activty to show newest additions
                startActivity(Intent(this, ChoreListActivity::class.java))
                finish()
            } else {
                Toast.makeText(this, "Please fill all fields", Toast.LENGTH_SHORT).show()
            }
        }
    }
    fun saveToDB(chore: Chore) {
        dbHandler!!.createChore(chore)
    }
}
