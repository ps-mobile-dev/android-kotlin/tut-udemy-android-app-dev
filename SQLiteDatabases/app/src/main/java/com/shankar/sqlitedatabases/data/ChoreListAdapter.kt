package com.shankar.sqlitedatabases.data

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AlertDialog
import android.support.v7.view.menu.ActionMenuItemView
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.shankar.sqlitedatabases.R
import com.shankar.sqlitedatabases.activity.ChoreListActivity
import com.shankar.sqlitedatabases.model.Chore
import kotlinx.android.synthetic.main.popup.view.*

class ChoreListAdapter(private val list: ArrayList<Chore>, private val context: Context): RecyclerView.Adapter<ChoreListAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder{
        //Creates view from XML file
        val view = LayoutInflater.from(context).inflate(R.layout.chore_list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        holder!!.bindViews(list[position])
    }
    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var mContext = context
        var choreName = itemView.findViewById(R.id.listChoreName) as TextView
        var assignedTo = itemView.findViewById(R.id.listChoreAssignedTo) as TextView
        var assignedBy = itemView.findViewById(R.id.listAssignedBy) as TextView
        var assignedTime = itemView.findViewById(R.id.listAssignedTime) as TextView
        var editChoreButton = itemView.findViewById(R.id.listEditChore) as Button
        var deleteChoreButton = itemView.findViewById(R.id.listDeleteChore) as Button

        fun bindViews(chore: Chore){
            choreName.text = chore.choreName
            assignedTo.text = "Assigned to: " + chore.assignedTo
            assignedBy.text = "Assigned by: " + chore.assignedBy
            assignedTime.text ="Created on: " +  chore.showHumanDate(chore.timeAssigned!!)
            editChoreButton.setOnClickListener(this)
            deleteChoreButton.setOnClickListener(this)
        }
        override fun onClick(view: View?) {
            var mPosition: Int= adapterPosition
            var chore = list[mPosition]
            when(view!!.id){
                deleteChoreButton.id -> {
                    deleteChore(chore.choreId!!.toInt())
                }
                editChoreButton.id -> {
                    editChore(chore)
                }
            }
        }

        private fun editChore(chore: Chore) {
            var dialogBuilder: AlertDialog.Builder? = null
            var dialog: AlertDialog? = null
            var dbHandler = ChoresDatabaseHandler(context)

            var view = LayoutInflater.from(context).inflate(R.layout.popup, null)
            var choreName = view.popEnterChore
            var assignedBy = view.popAssignedBy
            var assignedTo = view.popAssignTo
            var saveButton = view.popSaveChore
            dialogBuilder = AlertDialog.Builder(context).setView(view)
            dialog = dialogBuilder!!.create()
            dialog!!.show()
            saveButton.setOnClickListener {
                if (!TextUtils.isEmpty(choreName.text.trim())
                        && !TextUtils.isEmpty(assignedBy.text.trim())
                        && !TextUtils.isEmpty(assignedTo.text.trim())) {
                    chore.choreName = choreName.text.toString().trim()
                    chore.assignedTo = assignedTo.text.toString().trim()
                    chore.assignedBy = assignedBy.text.toString().trim()
                    dbHandler!!.updateChore(chore)
                    notifyItemChanged(adapterPosition, chore)
                    dialog!!.dismiss()

                } else {
                    Toast.makeText(context, "Please fill all fields", Toast.LENGTH_SHORT).show()
                }
            }
        }

        fun deleteChore(id: Int){
            var db = ChoresDatabaseHandler(context)
            db.deleteChore(id)
            list.removeAt(adapterPosition)
            notifyItemRemoved(adapterPosition)
        }
    }
}