package com.shankar.randombackground

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var colorArray = arrayOf(Color.BLACK,  Color.DKGRAY, Color.RED, Color.YELLOW, Color.CYAN, Color.GREEN)

        button.setOnClickListener {
            view.setBackgroundColor(colorArray[getRandom(colorArray.size)])
        }
    }

    fun getRandom(arraySize: Int): Int{
        var rand = Random()
        var randomNum = rand.nextInt(arraySize)
        return  randomNum
    }
}
