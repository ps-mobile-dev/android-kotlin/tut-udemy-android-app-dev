package com.shankar.volleyapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    var volleyRequest: RequestQueue? = null
    val stringLink = "https://www.magadistudio.com" +
            "/complete-android-developer-course-source-files/string.html"

    val movieLink = "https://www.netflixroulette.net/api/api.php?director=Quentin%20Tarantino"
    val heroesLink = "https://mdn.github.io/learning-area/javascript/oojs/json/superheroes.json"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        volleyRequest = Volley.newRequestQueue(this)
//        getString(stringLink)
//        getJSONArray(movieLink)
        getJSONObject(heroesLink)
    }

    fun getString(url: String) {

        //Volley StringRequest(method, utl, listner, errorListner)

        val stringRequest = StringRequest(Request.Method.GET, url, Response.Listener { response: String ->
            try {
                Log.d("Response", response)
                textView.text = response
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { error: VolleyError? ->
            try {
                Log.e("Volley Error", error.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
        volleyRequest!!.add(stringRequest)
    }

    fun getJSONArray(url: String) {
        val jsonArrayRequest = JsonArrayRequest(Request.Method.GET, url, Response.Listener { response: JSONArray ->
            try {
                Log.d("RESPONSE : ===> ", response.toString())

                for (i in 0..response.length() - 1) {
                    val movieObj = response.getJSONObject(i)
                    var showTitle = movieObj.getString("show_title")
                    Log.d("Moview Title : ", showTitle)
                }

            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { error: VolleyError? ->
            try {
                Log.d("Error : ", error.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
        volleyRequest!!.add(jsonArrayRequest)
    }

    fun getJSONObject(url: String) {
        var jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, Response.Listener { response: JSONObject ->
            try {
                var base = response.getString("secretBase")
                Log.d("Secret Base: ", base)

                var members = response.getJSONArray("members")
                for (i in 0..members.length() - 1) {
                    val membersObj = members.getJSONObject(i)
                    var showName = membersObj.getString("name")
                    Log.d("Moview Title : ", showName)
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }, Response.ErrorListener { error: VolleyError? ->
            try {
                Log.d("Error: ", error.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
        volleyRequest!!.add(jsonObjectRequest)

    }
}
